from restkit import Resource
import json
import logging

class Redmine(Resource):
    """An implementation of the Redmine REST API."""
    def __init__(self, url, apikey, pool_instance=None, **kwargs):
        self.endpoint = url
        self.apikey = apikey
        self.headers = self.make_headers({
            "X-Redmine-API-Key": self.apikey})

        super(Redmine, self).__init__(self.endpoint,
                                         follow_redirect=True,
                                         max_follow_redirect=10,
                                         **kwargs)
        self.setup_logging()


#    def request(self, *args, **kwargs):
#        resp = super(RedmineAPI, self).request(*args, **kwargs)
#        return json.loads(resp.body_string())

    def setup_logging(self, level=None, handler=None, formatter=None):
        self.log = logging.getLogger("Redmine")
        ch = handler or logging.StreamHandler()
        level = level or logging.INFO
        f = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        formatter = formatter or logging.Formatter(f)

        ch.setLevel(level)
        ch.setFormatter(formatter)
        self.log.addHandler(ch)

    def disable_logging(self, level=logging.CRITICAL):
        logging.disable(level)

    def enable_logging(self, level=logging.NOTSET):
        logging.disable(level)

    def issues(self, **kwargs):
        self.log.debug("issue(**kwargs) called")
        resp = self.get("/issues.json", headers=self.headers, **kwargs)
        body = resp.body_string()
        self.log.debug("GET response: {0}".format(body))
        ret = []

        try:
            issue_list = json.loads(body)["issues"]
        except KeyError:
            issue_list = []

        for i in issue_list:
            ret.append(Issue(i))

        return ret



class CustomDict(dict):
    def __init__(self, data):
        d = data.copy()
        self.__dict__ = self

        for key, value in data.iteritems():
            if isinstance(value, dict):
                d[key] = CustomDict(value)

        map(self.__setitem__, d.keys(), d.values())


class Issue(CustomDict):
    def __init__(self, data):
        super(Issue, self).__init__(data)

    def __hash__(self):
        return self.id


class Project(CustomDict):
    pass


class User(CustomDict):
    pass
