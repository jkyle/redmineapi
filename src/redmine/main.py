import sys
import os
import json
import argparse
import ConfigParser
from redmine.models import Redmine

def main():
    parser = get_parser()
    args = vars(parser.parse_args(sys.argv[1:]))
    config = get_config(args["config"])
    apikey = args.get("apikey", None) or config.get("redmine","apikey",
                                                     default=None)
    url = args.get("url", None) or config.get("redmine","url",default=None)
    validate(url=url, apikey=apikey)

    config.set("redmine", "url", url)
    config.set("redmine", "apikey", apikey)

    api    = Redmine(url, apikey)
    dispatch = {
        "issues": api.issues
    }

    subargs = get_subargs(args)
    result = dispatch[args["which"]](**subargs)

    # save new args to config file if requested
    if args.get("save", None):
        save_config(config, args["config"])
    print([i.project for i in result])
    #return json.dumps(result)

def get_subargs(args):
    dispatch = {
        "issues": get_issues_args
    }

    return dispatch[args["which"]](args)

def get_issues_args(args):
    subargs = {}
    for a in ["limit", "offset", "sort", "project_id", "tracker_id", 
              "status_id", "assigned_to_id", "cf_x"]:
        if args.get(a, None):
            subargs[a] = args[a]
    return subargs

def save_config(config, config_file):
    with open(config_file, "wb") as f:
        config.write(f)

def with_default(func):
    def wrapped(*args, **kwargs):
        gkwargs = kwargs.copy()
        if gkwargs.has_key("default"):
            del kwargs["default"]

        try:
            result = func(*args, **kwargs)
        except ConfigParser.NoOptionError as e:
            if gkwargs.has_key("default"):
                result = gkwargs["default"]
            else:
                raise e
        return result
    return wrapped

def get_parser():
    parser = argparse.ArgumentParser(description="Redmine API CLI")

    parser.add_argument("-a", "--apikey", default=None,
                        help="Redmine API Key. [found on your account page]")
    parser.add_argument("-u", "--url", default=None,
                        help="Redmine API End Point")
    parser.add_argument("-s", "--save", action="store_true",
                        help="save any set values to the user config file")
    parser.add_argument("-c", "--config",
                        default=os.path.expanduser("~/.redmine.cfg"),
                        help="specify a config file")

    subparsers = parser.add_subparsers(title="subcommands",
                                       description="List of redmine subcommands",
                                       help="additional help")

    # Issues subcommand arguments
    issues = subparsers.add_parser("issues")
    issues.set_defaults(which="issues")
    issues.add_argument("--limit", type=int, default=None,
                        help="limit the number of issues")
    issues.add_argument("--offset", type=int, default=None,
                        help="pagination offset")
    h = "sort the output by specified column. append :desc to invert order"
    issues.add_argument("--sort", default=None, 
                        help=h)
    issues.add_argument( "--project_id", default=None,
                        help="specify a project id to filter by")
    issues.add_argument( "--tracker_id", type=int, default=None,
                        help="specify a tracker id to filter by")
    issues.add_argument("--status_id", type=int, default=None,
                        help="specify a status id to filter by")
    issues.add_argument("--assigned_to_id", type=int, default=None,
                        help="specify an assignee id to filter by")
    issues.add_argument("--cf_x", default=None,
                        help="specify a custom field value to filter by")

    return parser

def validate(apikey=None, url=None):
    if not apikey:
        msg = "You must provide an apikey!\n"
        sys.stderr.write(msg)
        sys.exit(1)

    if not url:
        msg = "You must provide a Redmine url!\n"
        sys.stderr.write(msg)
        sys.exit(1)


def get_config(config_file):
    config = ConfigParser.SafeConfigParser()
    config.get = with_default(config.get)
    if not os.path.exists(config_file):
        config.add_section('redmine')
        config.set('redmine', 'apikey', '')
        config.set('redmine', 'url', '')

        with open(config_file, "wb") as f:
            config.write(f)

    config.read([os.path.expanduser("~/.redmine.cfg")])

    return config
