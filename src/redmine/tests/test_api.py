import json
import logging
from mock import Mock
from redmine.models import Redmine
from nose.tools import eq_

class TestRedmine:
    def setUp(self):
        self.api = Redmine(url="http://mock.com/restapi", apikey="fake")
        self.api.enable_logging()
        self.headers = {"X-Redmine-API-Key": "fake"}
        self.log = logging.getLogger("Redmine")
        self.mock = Mock()
        self.issues = {"issues": [{
                u'status': {u'name': u'New', u'id': 1},
                u'project': {u'name': u'Silver Lining', u'id': 95},
                u'description': u'Please create projects mysql-dev and mysql-prod in EWR1 and DFW1.\r\n\r\nThe only user for now will be gm2391.',
                u'author': {u'name': u'clauden', u'id': 138},
                u'id': 7582,
                u'priority': {u'name': u'Normal', u'id': 4},
                u'created_on':
                u'2012/01/28 02:01:08 -0500',
                u'tracker': {u'name': u'Task', u'id': 3},
                u'assigned_to': {u'name': u'jtran', u'id': 263},
                u'updated_on':
                u'2012/01/28 02:01:08 -0500',
                u'start_date': u'2012/01/28',
                u'done_ratio': 0,
                u'subject': u'Create projects for MySQL'
        }]}

    def tearDown(self):
        pass

    def _mock_test(self, func, *args, **kwargs):
        """
        Takes a function 'func', calls that function with *args, **kwargs, then
        tests that the self.mock object received 'call_args'.

        func        function to call
        call_args   the arguments the mock object should receive
        *args       the *args to pass to 'func'
        **kwargs    the **kwargs to pass to 'func'

        returns     returns the result of ret = func(*args, **kwargs)
        """
        expected = kwargs.copy()
        expected["headers"] = self.headers
        ret = func(**kwargs)

        eq_(self.mock.called, True)
        eq_(self.mock.call_count, 1)
        eq_(self.mock.call_args, (args, expected))
        self.mock.reset_mock()
        return ret

    def test_issues(self):
        self.api.get = self.mock

        # test self.api.issues()
        resp = Mock()
        resp.body_string.return_value = json.dumps(self.issues)
        self.mock.return_value = resp
        call_args = (("issues.json",), {})
        ret = self._mock_test(self.api.issues, *call_args[0], **call_args[1])
        eq_(len(ret), 1)
        eq_(ret[0].id, 7582)

        # test self.api.issues(offset=10)
        resp = Mock()
        resp.body_string.return_value = json.dumps(self.issues)
        self.mock.return_value = resp
        call_args[1]["offset"] = 10
        ret = self._mock_test(self.api.issues, *call_args[0], **call_args[1])
        eq_(ret, self.issues["issues"])

        # test self.api.issues(offset=10, limit=10)
        resp = Mock()
        self.issues["issues"] = 2 * self.issues["issues"]
        resp.body_string.return_value = json.dumps(self.issues)
        self.mock.return_value = resp
        call_args[1]["limit"] = 10
        ret = self._mock_test(self.api.issues, *call_args[0], **call_args[1])
        eq_(ret, self.issues["issues"])

        # test self.api.issues(offset=10, limit=10, sort=True)
        call_args[1]["sort"] = True
        self._mock_test(self.api.issues, *call_args[0], **call_args[1])

        # test self.api.issues(offset=10, limit=10, sort=True, project_id=1)
        call_args[1]["project_id"] = 1
        self._mock_test(self.api.issues, *call_args[0], **call_args[1])

        # test self.api.issues(offset=10, limit=10, sort=True, project_id=1,
        #                      tracker_id=1)
        call_args[1]["tracker_id"] = 1
        self._mock_test(self.api.issues, *call_args[0], **call_args[1])

        # test self.api.issues(offset=10, limit=10, sort=True, project_id=1,
        #                      tracker_id=1, status_id=1)
        call_args[1]["status_id"] = 5
        self._mock_test(self.api.issues, *call_args[0], **call_args[1])

        # test self.api.issues(offset=10, limit=10, sort=True, project_id=1,
        #                      tracker_id=1, status_id=1, assigned_to_id=5)
        call_args[1]["assigned_to_id"] = 1
        self._mock_test(self.api.issues, *call_args[0], **call_args[1])

        # test self.api.issues(offset=10, limit=10, sort=True, project_id=1,
        #                      tracker_id=1, status_id=1, assigned_to_id=5,
        #                       cf_x=5)
        call_args[1]["cf_x"] = 1
        self._mock_test(self.api.issues, *call_args[0], **call_args[1])

