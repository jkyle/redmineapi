Redmine API
==========================

``redmine`` is an implementation of the Redmine REST API.


.. _`redmine`: http://blog.jameskyle.org

Credits
-------

- `Distribute`_
- `modern-package-template`_
- `mock`_
- `nose`_
- `coverage`_
- `restkit`_

.. _Distribute: http://pypi.python.org/pypi/distribute
.. _`modern-package-template`: http://pypi.python.org/pypi/modern-package-template
.. _`mock`: http://www.voidspace.org.uk/python/mock/ 
.. _`nose`: http://readthedocs.org/docs/nose/en/latest/
.. _`coverage`: http://nedbatchelder.com/code/coverage/
.. _`restkit`: http://benoitc.github.com/restkit/
